using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Threading.Tasks;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Collections.Generic;
using System.Text;
using QueryStorm.Apps;

namespace Windy.ExchangeRates.HNB
{
    public enum TipTecaja
    {
        Srednji = 0,
        Kupovni = 1,
        Prodajni = 2
    }

    public class ExcelFunctions1
    {
        private static Dictionary<TipTecaja, string> tipTecajaPropName = new Dictionary<TipTecaja, string>
        {
            { TipTecaja.Kupovni, "kupovni_tecaj" },
            { TipTecaja.Srednji, "srednji_tecaj" },
            { TipTecaja.Prodajni, "prodajni_tecaj" },
        };

        [ExcelFunction(Name = "Windy.ToHRK", Description = "Konvertira iznos iz zadane valute u hrvatske kune.")]
        public static async Task<double> ToHRK(double iznos, string fromCurrency, DateTime? date, TipTecaja tip = 0)
        {
            if (fromCurrency == "HRK")
                return iznos;

            date = date ?? DateTime.Today;

            int retriesRemaining = 2;

            for (int i = 0; i < retriesRemaining; i++)
            {
                try
                {
                    var requestUri = $"http://api.hnb.hr/tecajn/v2?datum-primjene={date.Value.ToString("yyyy-MM-dd")}&valuta={fromCurrency}";
                    var res = await new HttpClient().GetAsync(requestUri);
                    var response = await res.Content.ReadAsStringAsync();

                    var tecajStr = JArray.Parse(response)
                        .Single()
                        .Value<string>(tipTecajaPropName[(TipTecaja)tip]);

                    var nfi = new NumberFormatInfo() { NumberDecimalSeparator = "," };
                    var tecaj = double.Parse(tecajStr, nfi);

                    return tecaj * iznos;
                }
                catch
                {
                    retriesRemaining--;
                }
            }

            throw new Exception();
        }
    }
}
